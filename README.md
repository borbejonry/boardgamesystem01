# README #

Game mechanics:
- Using a Dice or Super Dice, race with an AI to win

Win Condition:
- Reach a certain tile.

Controller:
- Click the Normal Dice or Super Dice
- Press "Space Bar" to reset game.
- Press "Esc" to exit app.