using UnityEngine;

public class AppElement : MonoBehaviour
{
    public AppApplication App 
    { 
        get 
        { 
            return GameObject.FindObjectOfType<AppApplication>(); 
        } 
    }
}
