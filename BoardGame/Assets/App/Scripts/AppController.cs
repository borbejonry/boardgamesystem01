using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppController : AppElement
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            App.ResetAndStartGame();
            App.StopAllRoutines();
        }

#if UNITY_WEBGL

#else
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
#endif
    }

    public void ThrowNormalDice(Player _character)
    {
        //Normal dice
        int _val = _character.NormalDice();
        Debug.Log($"ThrowNormalDice: {_val} {true}");
        App.view.MoveBasedOnDice(_character, _val, true);


    }
    public void ThrowSuperDice(Player _character)
    {
        //Super dice
        int _val = _character.SuperDice();
        Debug.Log($"ThrowSuperDice: {_val} {false}");
        App.view.MoveBasedOnDice(_character, _val, false);
    }
}
