using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    [SerializeField] Renderer rend;

    //-1 = not available, 0 = default, 1 = pass
    int availability = 0;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetAvailability(int _available)
    {
        switch (_available)
        {
            case -1:
                SetNotAvailable();
                break;
            case 1:
                //rend.material.color = new Color(0,0.5f,0,1);
                //availability = -1;
                break;
            default:
                rend.material.color = Color.white;
                availability = 0;
                break;
        }
    }
    public void SetNotAvailable()
    {
        rend.material.color = Color.black;
        availability = -1;
    }

    public void SetFinishLineColor()
    {
        rend.material.color = Color.green;
    }

    public bool CheckAvailable()
    {
        if (availability != -1)
        {
            return true;
        }

        return false;
    }
}
