using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppModel : AppElement
{
	public bool isGameStart = false;

	public Player player;
	public Player enemy;

	public Player curCharacter;

	float gridSpace = 1.1f;
	public GameObject[,] grids;

	int width = 0;
	int height = 0;

	List<GameObject> pathFinding = new List<GameObject>();

	public int pathMaxCounter = 0;

	//(width,height)
	public void CreateGrid(int _gridX, int _gridY)
	{
		width = _gridX;
		height = _gridY;

		grids = new GameObject[width, height];

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				grids[x, y] = App.view.CreateCell(x * gridSpace, y * gridSpace);
				grids[x, y].gameObject.name += $" Cell: {x} , {y}";
			}
		}
	}

	public void StageOne()
    {
		//Inverse C Level
		for (int y = 1; y < (height-1); y++)
		{
			for (int x = 0; x < (width-1); x++)
			{
				grids[x, y].GetComponent<Cell>().SetNotAvailable();
			}
		}

		for (int x = 0; x < width; x++)
		{
			pathFinding.Add(grids[x, 0]);
		}

		for (int y = 1; y < height; y++)
		{
			pathFinding.Add(grids[width-1, y]);
		}

		for (int x = width-1; x >= 0; x--)
		{
			pathFinding.Add(grids[x, width - 1]);
		}

		pathFinding[pathFinding.Count - 1].GetComponent<Cell>().SetFinishLineColor();

		pathMaxCounter = pathFinding.Count;
	}

	public void CreatePlayer()
	{
		player = App.view.CreatePlayer(grids[0, 0]).GetComponent<Player>();
		player.grid = new Vector2(0,0);
		player.moveIndex = 0;
	}
	public void CreateEnemy()
	{
		enemy = App.view.CreateEnemy(grids[0, 0]).GetComponent<Player>();
		enemy.grid = new Vector2(0, 0);
		enemy.moveIndex = 0;
	}

	public void ResetData()
    {
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				Destroy(grids[x, y]);
			}
		}

		if (player != null)
		{
			Destroy(player.gameObject);
		}

		if (enemy != null)
		{
			Destroy(enemy.gameObject);
		}

		pathFinding.Clear();
	}

	public void MoveToNextGrid(Player character, int moveCount)
    {
        if (moveCount != 0)
        {
			character.moveIndex++;
            if (character.moveIndex >= pathFinding.Count)
            {
				character.moveIndex = pathFinding.Count - 1;
			}
			character.MoveToGrid(pathFinding[character.moveIndex]);
		}

	}
}
