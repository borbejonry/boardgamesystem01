using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Dice : MonoBehaviour
{
    public event Action<int, Player> OnComplete;

    [SerializeField] TextMeshProUGUI label;

    public void BeginRoll(int _finalValue, bool isNormal, Player character)
    {
        StartCoroutine(StartDice(_finalValue, isNormal, character));
    }

    IEnumerator StartDice(int _finalValue, bool isNormal, Player character)
    {
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < 120; i++)
        {
            yield return new WaitForEndOfFrame();
            if (isNormal)
            {
                label.text = UnityEngine.Random.Range(1,7).ToString();
            }
            else
            {
                label.text = UnityEngine.Random.Range(5, 11).ToString();
            }
        }

        yield return new WaitForEndOfFrame();
        label.text = "" + _finalValue;
        yield return new WaitForEndOfFrame();
        yield return new WaitForSeconds(0.5f);

        OnComplete?.Invoke(_finalValue, character);
    }
    public void StopAllRoutines()
    {
        StopAllCoroutines();
    }
}
