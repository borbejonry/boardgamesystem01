
using UnityEngine;
public interface IMovement
{
    int moveIndex { get; set; }
    void MoveToGrid(GameObject _movement);
}