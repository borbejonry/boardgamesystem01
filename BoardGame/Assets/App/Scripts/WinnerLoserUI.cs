using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WinnerLoserUI : MonoBehaviour
{
    public event Action OnClicked;

    [SerializeField] Button retryButton;
    [SerializeField] TextMeshProUGUI label;

    void Start()
    {
        retryButton.onClick.AddListener(Clicked);
    }
    private void OnDestroy()
    {
        retryButton.onClick.RemoveListener(Clicked);
    }

    private void Clicked()
    {
        OnClicked?.Invoke();
    }

    public void ChangeLabel(string val)
    {
        label.text = val;
    }
}
