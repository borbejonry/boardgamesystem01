using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class AppView : AppElement
{
    float speedUpdate = 0.125f;


    [Header("Grids")]
    [SerializeField] GameObject cloneToCell;
    [SerializeField] Transform gridParent;

    [Header("Players")]
    [SerializeField] GameObject cloneToPlayer;

    [Header("Enemies")]
    [SerializeField] GameObject cloneToEnemy;

    [Header("Audio")]
    [SerializeField] AudioSource audio;
    [SerializeField] AudioClip sfxJump;

    [Header("UI")]
    [SerializeField] Transform playerUI;
    [SerializeField] TextMeshProUGUI turnText;
    [SerializeField] Button btnSuperThrowCooldown;
    [SerializeField] Button btnNormalThrow;
    [SerializeField] GameObject diceToClone;

    [SerializeField] WinnerLoserUI winneLoserUI;

    Dice selfDice;
    private void OnDestroy()
    {
        btnNormalThrow.onClick.RemoveListener(NormalThrowClicked);
        btnSuperThrowCooldown.onClick.RemoveListener(SuperThrowClicked);

        winneLoserUI.OnClicked -= WinnerLoserClicked;
    }

    private void Start()
    {
        btnNormalThrow.onClick.AddListener(NormalThrowClicked);
        btnSuperThrowCooldown.onClick.AddListener(SuperThrowClicked);

        winneLoserUI.OnClicked += WinnerLoserClicked;
    }

    private void WinnerLoserClicked()
    {
        App.ResetAndStartGame();
    }

    public void EnableSuperDice(bool isOn)
    {
        btnSuperThrowCooldown.interactable = isOn;
    }
    public void EnableNormalDice(bool isOn)
    {
        btnNormalThrow.interactable = isOn;
    }

    private void SuperThrowClicked()
    {
        if (App.model.isGameStart)
        {
            if ((App.model.player.isTurn)&&(App.model.player.GetCooldownPercentage() == 1))
            {
                EnableNormalDice(false);
                App.model.player.isTurn = false;
                App.controller.ThrowSuperDice(App.model.player);
            }
        }
    }

    private void NormalThrowClicked()
    {
        if (App.model.isGameStart)
        {
            if (App.model.player.isTurn)
            {
                EnableNormalDice(false);
                App.model.player.isTurn = false;
                App.controller.ThrowNormalDice(App.model.player);
            }
        }
    }

    public GameObject CreateCell(float _x, float _y)
    {
        return Instantiate(cloneToCell, new Vector3(_x, 0, _y), Quaternion.identity, gridParent);
    }
    public GameObject CreatePlayer(GameObject toCell)
    {
        return Instantiate(cloneToPlayer, toCell.transform.position, Quaternion.identity);
    }
    public GameObject CreateEnemy(GameObject toCell)
    {
        return Instantiate(cloneToEnemy, toCell.transform.position, Quaternion.identity);
    }

    public void SetTextTurn(string _val)
    {
        if (App.model.curCharacter)
        {
            turnText.text = $"Turn: {_val}";
        }
        else
        {
            turnText.text = $"Turn: {_val}";
        }
    }

	public void MoveBasedOnDice(Player character, int moveCount, bool isNormal)
	{
        if (character == App.model.player)
        {
            App.view.PlayerCooldownUpdate();
        }

        InstantiateDice(moveCount, isNormal, character);
        //StartCoroutine(MovePerEndFrame(character, moveCount));
    }

    IEnumerator MovePerEndFrame(Player character, int moveCount)
    {
        Debug.Log("moveCount: " + moveCount);
        yield return new WaitForEndOfFrame(); yield return new WaitForSeconds(speedUpdate);
        App.model.MoveToNextGrid(character, moveCount);
        audio.clip = sfxJump;
        if (character == App.model.player)
        {
            audio.pitch = 2;
        }
        else
        {
            audio.pitch = 3;
        }

        audio.Play();

        if ( character.CheckWinner() )
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(speedUpdate);
            //win condition
            Debug.Log("win");
            if (character == App.model.player)
            {
                WinnerLoserScreen(true, "PLAYER WIN!");
            }
            else
            {
                WinnerLoserScreen(true, "ENEMY WIN!");
            }
        }
        else
        {
            moveCount--;
            if (moveCount == 0)
            {
                yield return new WaitForEndOfFrame();
                character.InvokeTurnComplete();
            }
            else
            {
                yield return new WaitForEndOfFrame(); yield return new WaitForSeconds(speedUpdate);
                StartCoroutine(MovePerEndFrame(character, moveCount));
            }
        }
	}

    public void PlayerCooldownUpdate()
    {
        btnSuperThrowCooldown.targetGraphic.GetComponent<Image>().fillAmount = App.model.player.GetCooldownPercentage();
    }

    public void WinnerLoserScreen(bool isOn, string val = "")
    {
        winneLoserUI.gameObject.SetActive(isOn);
        winneLoserUI.ChangeLabel( val );
    }

    void InstantiateDice(int _finalValue, bool isNormal, Player character)
    {
        Debug.Log($"{_finalValue} {isNormal} {character.moveIndex}");
        selfDice = Instantiate(diceToClone).GetComponent<Dice>();
        selfDice.OnComplete += DiceRollComplete;
        selfDice.BeginRoll(_finalValue, isNormal, character);
    }

    void DiceRollComplete(int moveCount, Player character)
    {
        selfDice.OnComplete -= DiceRollComplete;
        Destroy(selfDice.gameObject);

        StartCoroutine(MovePerEndFrame(character, moveCount));
    }

    public void StopAllRoutines()
    {
        if (selfDice != null)
        {
            selfDice.StopAllRoutines();
            Destroy(selfDice.gameObject);
        }
        StopAllCoroutines();
    }
}
