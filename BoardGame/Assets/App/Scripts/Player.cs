using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : AppElement, IMovement
{
    public event Action OnTurnComplete;

    //x = current turn, y = max cooldown turn
    Vector2 superDiceTurnCooldown = new Vector2(0, 3);
    public Vector2 grid;
    public bool isTurn = false;

    public int moveIndex { get; set; }
    public void MoveToGrid(GameObject moveTo)
    {
        this.gameObject.transform.position = moveTo.transform.position;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InvokeTurnComplete()
    {
        OnTurnComplete?.Invoke();
    } 

    public int NormalDice()
    {
        superDiceTurnCooldown.x++;

        if (superDiceTurnCooldown.x >= superDiceTurnCooldown.y)
        {
            superDiceTurnCooldown.x = superDiceTurnCooldown.y;
        }

        return UnityEngine.Random.Range(1, 7);
    }
    public int SuperDice()
    {
        superDiceTurnCooldown.x = 0;

        return UnityEngine.Random.Range(5, 11);
    }

    public bool CheckWinner()
    {
        if (moveIndex == (App.model.pathMaxCounter-1))
        {
            return true;
        }

        return false;
    }

    public void AISuperDice()
    {
        if (superDiceTurnCooldown.x == superDiceTurnCooldown.y)
        {
            Debug.Log($"AISuperDice {moveIndex}");
            App.controller.ThrowSuperDice(this);

        }
        else
        {
            App.controller.ThrowNormalDice(this);
        }
    }

    public float GetCooldownPercentage()
    {
        return (superDiceTurnCooldown.x / superDiceTurnCooldown.y);
    }
}
