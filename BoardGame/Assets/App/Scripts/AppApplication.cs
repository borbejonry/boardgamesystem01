using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppApplication : MonoBehaviour
{
    public AppModel model;
    public AppView view;
    public AppController controller;

    void Start()
    {
        ResetAndStartGame();
    }

    public void ResetAndStartGame()
    {
        view.WinnerLoserScreen(false, "");

        if (model.player != null)
        {
            model.player.OnTurnComplete -= PlayerTurnComplete;
        }
        if (model.enemy != null)
        {
            model.enemy.OnTurnComplete -= EnemyTurnComplete;
        }

        model.ResetData();
        view.EnableNormalDice(true);

        model.CreateGrid(10, 10);
        model.StageOne();
        model.CreatePlayer();
        model.CreateEnemy();

        if (model.player != null)
        {
            model.player.OnTurnComplete += PlayerTurnComplete;
        }
        if (model.enemy != null)
        {
            model.enemy.OnTurnComplete += EnemyTurnComplete;
        }

        //startgame
        model.isGameStart = true;
        model.curCharacter = model.player;
        model.curCharacter.isTurn = true;
        view.PlayerCooldownUpdate();
        view.SetTextTurn("Player");
    }

    private void PlayerTurnComplete()
    {
        model.player.isTurn = false;
        model.enemy.isTurn = true;
        view.EnableNormalDice(false);
        view.EnableSuperDice(false);

        model.curCharacter = model.enemy;
        view.SetTextTurn("Enemy");

        model.curCharacter.AISuperDice();
    }
    private void EnemyTurnComplete()
    {
        model.enemy.isTurn = false;
        model.player.isTurn = true;
        view.EnableNormalDice(true);
        view.EnableSuperDice(true);

        model.curCharacter = model.player;
        view.SetTextTurn("Player");

        if (model.player.GetCooldownPercentage() == 1)
        {
            view.EnableSuperDice(true);
        }
    }

    public void StopAllRoutines()
    {
        view.StopAllRoutines();
    }
}
